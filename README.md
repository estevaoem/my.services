# Microservices Demo

## Description

This is the repo containing all the services described in [this](./docs/my-services-signup-flow.png) schema. The goal of this project is to demonstrate skills and for studying purposes of certain microservices use cases.

## Dependencies

- [NodeJS V18](https://github.com/nvm-sh/nvm)
- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Services

- [users](./ms.users/README.md)
- [signup](./ms.signup/README.md)

## Design choices

### Testing db within containers

Even though there are solutions like mongo in memory, which are simpler than the present for testing, I've chosen not to do so for one reason: for databases that don't offer that kind of solution, testing code related to db interactions is subjected to failure, because most of these interactions will be mocked and changes may not be equally reflected for this mocks.

For those tests that require the usage of containers, the files are marked with a `*.containerOnly.*`, so pipe validations that don't support a complex environment don't fall off.

### Mono repo, common domain and docs folder

There is not much complexity in this repo, nor a big team (it's just me) to justify the usage of git submodules. Adding such integration requires upstream fixes that slow considerably the development. Unifying everything in a single repo makes it easier to make changes, mainly in the communication protocols between services.

This, of course, thinking on an initial version of the services. As the project matures, the com protocol will be mature as well, requiring minimal intervention. In a need for newer protocols, newer API versions are released to acomodate this new com protocol.