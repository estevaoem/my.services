/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import * as rxjs from 'rxjs';
import { Observable } from 'rxjs';
import { CreateUser, CreateUserDto } from '../../../../domain/dtos/create.user';
import { PreUser } from 'src/pre.user/data/pre.user.model';
import { environment } from '../../common/environment';
import { UserModel } from '../data/user.model';
import { ITokenGenerator } from '../infra/itoken.generator';
import { SignupService } from './signup.service';
import { IPreUserService } from 'src/pre.user/ipre.user.service';
import { makeUser } from '../data/user.model.factory';
import { ClientProxyStub } from '../../common/client.proxy.stub';
import { PreUserServiceStub } from '../../pre.user/ipre.user.service.stub';
import { makePreUser } from '../../pre.user/data/pre.user.model.factory';
import { TokenGeneratorStub } from '../infra/itoken.generator.stub';
import { faker } from '@faker-js/faker';
import {
  makeCreateUser,
  makeCreateUserDto,
} from '../../../../domain/dtos/create.user.factory';

const makeThrow = (
  components: SutComponents,
  component: keyof SutComponents,
  method: string,
): void => {
  jest
    .spyOn(components[component], method as never)
    .mockImplementationOnce((...args: never): never => {
      throw new Error(`Error with signupService ${method} method`);
    });
};

interface SutComponents {
  preUserService: IPreUserService;
  client: ClientProxyStub;
  tokenGenerator: ITokenGenerator;
}

describe('SignupService', () => {
  let sut: SignupService;
  let components: SutComponents;
  let user: UserModel;
  let preUser: PreUser;
  let id: string;

  beforeEach(async () => {
    user = makeUser();
    preUser = makePreUser();
    id = faker.database.mongodbObjectId();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SignupService,
        {
          provide: 'IPreUserService',
          useClass: PreUserServiceStub,
        },
        {
          provide: 'SIGNUP_SERVICE',
          useClass: ClientProxyStub,
        },
        {
          provide: 'ITokenGenerator',
          useClass: TokenGeneratorStub,
        },
      ],
    }).compile();

    sut = module.get<SignupService>(SignupService);
    components = {
      preUserService: module.get<IPreUserService>('IPreUserService'),
      client: module.get<ClientProxyStub>('SIGNUP_SERVICE'),
      tokenGenerator: module.get<ITokenGenerator>('ITokenGenerator'),
    };
  });

  describe('check user existis', () => {
    it('should call client proxy with correct values', async () => {
      const sendSpy = jest.spyOn(components.client, 'send');
      const pipeSpy = jest.spyOn(components.client, 'pipe');
      jest
        .spyOn(rxjs, 'timeout')
        .mockImplementationOnce(
          (
            each: number,
            scheduler?: rxjs.SchedulerLike,
          ): rxjs.MonoTypeOperatorFunction<unknown> => {
            return each as unknown as rxjs.MonoTypeOperatorFunction<unknown>;
          },
        );
      const toPromiseSpy = jest.spyOn(components.client, 'toPromise');

      await sut.checkUserExists(user.email);
      expect(sendSpy).toHaveBeenCalledWith(
        { cmd: 'check_user_exists' },
        user.email,
      );
      expect(toPromiseSpy).toHaveBeenCalledWith();
      expect(pipeSpy).toHaveBeenCalledWith(
        environment.SERVICE_RESPONSE_TIMEOUT,
      );
    });

    it('should throw if client throws', async () => {
      jest
        .spyOn(components.client, 'send')
        .mockImplementationOnce(
          (pattern: any, data: unknown): Observable<unknown> => {
            throw new Error('Error with signupService client send method');
          },
        );
      const userExistsSend = sut.checkUserExists(user.email);
      expect(userExistsSend).rejects.toThrow();

      makeThrow(components, 'client', 'toPromise');
      const userExistsToPromise = sut.checkUserExists(user.email);
      expect(userExistsToPromise).rejects.toThrow();
    });

    it('should return a boolean on success', async () => {
      const checkUserExists = await sut.checkUserExists(user.email);
      expect(checkUserExists).toEqual(true);
    });
  });

  describe('pre register user', () => {
    it('should call pre user service with correct values', async () => {
      const preUserRegisterSpy = jest.spyOn(
        components.preUserService,
        'preRegisterUser',
      );
      jest
        .spyOn(components.tokenGenerator, 'generate')
        .mockReturnValueOnce('1234');

      await sut.preRegisterUser(user.email);
      expect(preUserRegisterSpy).toHaveBeenCalledWith({
        email: user.email,
        token: '1234',
      });
    });

    it('should throw if pre user service throws', async () => {
      makeThrow(components, 'preUserService', 'preRegisterUser');
      const preUserCreationResult = sut.preRegisterUser(user.email);
      expect(preUserCreationResult).rejects.toThrow();
    });

    it('should emit an event to comunitcate user registration', async () => {
      const emitSpy = jest.spyOn(components.client, 'emit');
      jest
        .spyOn(components.tokenGenerator, 'generate')
        .mockReturnValueOnce('0666');
      await sut.preRegisterUser(user.email);
      expect(emitSpy).toHaveBeenCalledWith('send_com', {
        type: 'token',
        via: 'email',
        payload: {
          email: user.email,
          token: '0666',
        },
      });
    });

    it('should throw if client throws', async () => {
      jest
        .spyOn(components.client, 'emit')
        .mockImplementationOnce(
          (pattern: any, data: unknown): Observable<unknown> => {
            throw new Error('Error');
          },
        );
      const preUserCreationResult = sut.preRegisterUser(user.email);
      expect(preUserCreationResult).rejects.toThrow();
    });

    it('should return a pre user id on success', async () => {
      const preUserId = await sut.preRegisterUser(user.email);
      expect(preUserId).toEqual('pre_user_id');
    });
  });

  describe('resend token', () => {
    it('should call pre user service with correct values', async () => {
      const getPreUserSpy = jest.spyOn(components.preUserService, 'getPreUser');

      await sut.resendToken(id);
      expect(getPreUserSpy).toHaveBeenCalledWith({ id });
    });

    it('should not emit an event to resend token if a user is not found', async () => {
      const emitSpy = jest.spyOn(components.client, 'emit');
      jest
        .spyOn(components.preUserService, 'getPreUser')
        .mockResolvedValueOnce(null);
      await sut.resendToken(id);
      expect(emitSpy).not.toHaveBeenCalled();
    });

    it('should throw if client throws', async () => {
      jest
        .spyOn(components.client, 'emit')
        .mockImplementationOnce(
          (pattern: any, data: unknown): Observable<unknown> => {
            throw new Error('Error');
          },
        );
      const resendTokenResult = sut.resendToken(user.email);
      expect(resendTokenResult).rejects.toThrow();
    });

    it('should emit an event to resend token on success', async () => {
      const emitSpy = jest.spyOn(components.client, 'emit');
      const foundUser = makePreUser();
      jest
        .spyOn(components.preUserService, 'getPreUser')
        .mockResolvedValueOnce(foundUser);
      await sut.resendToken(id);
      expect(emitSpy).toHaveBeenCalledWith('send_com', {
        type: 'token',
        via: 'email',
        payload: {
          email: foundUser.email,
          token: foundUser.token,
        },
      });
    });
  });

  describe('invalidate token', () => {
    it('should call pre user service with correct values', async () => {
      const updatePreUserSpy = jest.spyOn(
        components.preUserService,
        'invalidateToken',
      );

      await sut.invalidateToken(preUser.id.toString());
      expect(updatePreUserSpy).toHaveBeenCalledWith(preUser.id);
    });

    it('should throw if pre user service throws', async () => {
      makeThrow(components, 'preUserService', 'invalidateToken');
      const preUserCreationResult = sut.invalidateToken(preUser.id.toString());
      expect(preUserCreationResult).rejects.toThrow();
    });
  });

  describe('create user', () => {
    let createUser: CreateUser;
    beforeEach((): void => {
      createUser = makeCreateUser();
    });
    it('should call client proxy send with correct values', async () => {
      const sendSpy = jest.spyOn(components.client, 'send');
      const pipeSpy = jest.spyOn(components.client, 'pipe');
      jest
        .spyOn(rxjs, 'timeout')
        .mockImplementationOnce(
          (
            each: number,
            scheduler?: rxjs.SchedulerLike,
          ): rxjs.MonoTypeOperatorFunction<unknown> => {
            return each as unknown as rxjs.MonoTypeOperatorFunction<unknown>;
          },
        );
      const toPromiseSpy = jest.spyOn(components.client, 'toPromise');

      await sut.createUser(createUser);
      expect(sendSpy).toHaveBeenCalledWith(
        { cmd: 'create_user' },
        {
          email: createUser.email,
          password: createUser.password,
          passwordConfirmation: createUser.passwordConfirmation,
          plan: 'free',
        },
      );
      expect(toPromiseSpy).toHaveBeenCalledWith();
      expect(pipeSpy).toHaveBeenCalledWith(
        environment.SERVICE_RESPONSE_TIMEOUT,
      );
    });

    it('should throw if client send throws', async () => {
      jest
        .spyOn(components.client, 'send')
        .mockImplementationOnce(
          (pattern: any, data: unknown): Observable<unknown> => {
            throw new Error('Error with signupService client send method');
          },
        );
      const userExistsSend = sut.createUser(createUser);
      expect(userExistsSend).rejects.toThrow();

      makeThrow(components, 'client', 'toPromise');
      const userExistsToPromise = sut.createUser(createUser);
      expect(userExistsToPromise).rejects.toThrow();
    });

    it('should return a failure on user creation failure', async () => {
      jest
        .spyOn(components.client, 'toPromise')
        .mockReturnValueOnce(
          new Promise((resolve) =>
            resolve({ status: 'failure', anything: 'else' }),
          ),
        );
      const emitSpy = jest.spyOn(components.client, 'emit');
      const checkUserExists = await sut.createUser(createUser);
      expect(checkUserExists).toEqual({ status: 'failure', anything: 'else' });
      expect(emitSpy).not.toHaveBeenCalled();
    });

    it('should call client proxy emit with correct values', async () => {
      const emitSpy = jest.spyOn(components.client, 'emit');

      await sut.createUser(createUser);
      expect(emitSpy).toHaveBeenCalledWith('create_user', createUser);
    });

    it('should throw if client emit throws', async () => {
      jest
        .spyOn(components.client, 'emit')
        .mockImplementationOnce(
          (pattern: any, data: unknown): Observable<unknown> => {
            throw new Error('Error with signupService client emit method');
          },
        );
      const createUserResult = sut.createUser(createUser);
      expect(createUserResult).rejects.toThrow();
    });

    it('should return user creation result on success', async () => {
      const createUserResult = await sut.createUser(createUser);
      expect(createUserResult).toEqual({ defined: 'object' });
    });
  });
});
