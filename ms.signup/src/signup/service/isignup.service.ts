import { CreateUser } from '../../../../domain/dtos/create.user';
import { OperationStatus } from '../../../../domain/dtos/operation';
import { Id } from '../../../../domain/types';
import { PreUser } from 'src/pre.user/data/pre.user.model';

export interface CreatedUserOperationStatus extends OperationStatus {
  payload?: { createdUserId: string };
}

export interface ISignupService {
  checkUserExists(credential: Id): Promise<boolean>;
  preRegisterUser(credential: Id): Promise<Id>;
  getPreUser(preUser: Partial<PreUser>): Promise<PreUser | null>;
  invalidateToken(credential: Id): Promise<void>;
  createUser(user: CreateUser): Promise<CreatedUserOperationStatus>;
  resendToken(preUserId: string): Promise<void>;
}
