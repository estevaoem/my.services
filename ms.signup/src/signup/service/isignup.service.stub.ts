/* eslint-disable @typescript-eslint/no-unused-vars */
import { CreateUser } from '../../../../domain/dtos/create.user';
import { Id } from '../../../../domain/types';
import { PreUser } from 'src/pre.user/data/pre.user.model';
import { makePreUser } from '../../pre.user/data/pre.user.model.factory';
import { CreatedUserOperationStatus, ISignupService } from './isignup.service';
import { faker } from '@faker-js/faker';

export class SignupServiceStub implements ISignupService {
  async getPreUser(preUser: Partial<PreUser>): Promise<PreUser> {
    const madePreUser = makePreUser();
    return {
      ...madePreUser,
      ...preUser,
    };
  }
  async checkUserExists(credential: Id): Promise<boolean> {
    return false;
  }
  async preRegisterUser(credential: Id): Promise<Id> {
    return 'created_pre_user_id';
  }
  async invalidateToken(credential: Id): Promise<void> {
    return;
  }
  async createUser(user: CreateUser): Promise<CreatedUserOperationStatus> {
    return {
      status: 'ok',
      payload: {
        createdUserId: faker.database.mongodbObjectId(),
      },
    };
  }
  async resendToken(credential: Id): Promise<void> {
    return;
  }
}
