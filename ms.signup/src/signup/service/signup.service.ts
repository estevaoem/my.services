import { Inject, Injectable } from '@nestjs/common';
import { CreatedUserOperationStatus, ISignupService } from './isignup.service';
import { ClientProxy } from '@nestjs/microservices';
import { SendComunication } from '../../../../domain/dtos/coms';
import { ITokenGenerator } from '../infra/itoken.generator';
import { environment } from '../../common/environment';
import { CreateUser } from '../../../../domain/dtos/create.user';
import { timeout } from 'rxjs';
import { IPreUserService } from 'src/pre.user/ipre.user.service';
import { Id } from '../../../../domain/types';
import { PreUser } from 'src/pre.user/data/pre.user.model';

@Injectable()
export class SignupService implements ISignupService {
  constructor(
    @Inject('IPreUserService') private readonly preUserService: IPreUserService,
    @Inject('SIGNUP_SERVICE') private client: ClientProxy,
    @Inject('ITokenGenerator') private tokenGenerator: ITokenGenerator,
  ) {}
  async getPreUser(preUser: Partial<PreUser>): Promise<PreUser | null> {
    return await this.preUserService.getPreUser(preUser);
  }

  async checkUserExists(credential: Id): Promise<boolean> {
    const userExists = await this.client
      .send({ cmd: 'check_user_exists' }, credential)
      .pipe(timeout(environment.SERVICE_RESPONSE_TIMEOUT))
      .toPromise();
    return Boolean(userExists);
  }

  async preRegisterUser(email: string): Promise<Id> {
    const token = this.tokenGenerator.generate(environment.TOKEN_SIZE);
    const preUserId = await this.preUserService.preRegisterUser({
      email,
      token,
    });
    const com: SendComunication = {
      type: 'token',
      via: 'email',
      payload: {
        email,
        token,
      },
    };
    this.client.emit('send_com', com);
    return preUserId;
  }

  async resendToken(preUserId: string): Promise<void> {
    const preUser = await this.preUserService.getPreUser({ id: preUserId });
    if (!preUser) {
      return;
    }
    const { email, token } = preUser;
    const com: SendComunication = {
      type: 'token',
      via: 'email',
      payload: {
        email,
        token,
      },
    };
    this.client.emit('send_com', com);
  }

  async invalidateToken(preUserId: string): Promise<void> {
    await this.preUserService.invalidateToken(preUserId);
  }

  async createUser(user: CreateUser): Promise<CreatedUserOperationStatus> {
    const userCreationResult = await this.client
      .send<CreatedUserOperationStatus>(
        { cmd: 'create_user' },
        {
          ...user,
          plan: 'free',
        },
      )
      .pipe(timeout(environment.SERVICE_RESPONSE_TIMEOUT))
      .toPromise();

    if (userCreationResult.status === 'failure') {
      return userCreationResult;
    }

    this.client.emit('create_user', user);

    return userCreationResult;
  }
}
