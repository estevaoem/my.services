import { Module } from '@nestjs/common';
import { SignupController } from './controller/signup.controller';
import { SignupService } from './service/signup.service';
import { TokenGenerator } from './infra/token.generator';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { environment } from '../common/environment';
import { PreUserModule } from '../pre.user/pre.user.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'SIGNUP_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [
            `amqp://${environment.MESSAGE_BROKER_USER}:${environment.MESSAGE_BROKER_PASSWORD}@${environment.MESSAGE_BROKER_HOST}:${environment.MESSAGE_BROKER_PORT}`,
          ],
          queue: 'user_creation_queue',
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
    AuthModule,
    PreUserModule,
  ],
  controllers: [SignupController],
  providers: [
    {
      provide: 'ISignupService',
      useClass: SignupService,
    },
    {
      provide: 'ITokenGenerator',
      useClass: TokenGenerator,
    },
  ],
})
export class SignupModule {}
