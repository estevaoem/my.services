/* eslint-disable @typescript-eslint/no-unused-vars */
import { SignupController } from './signup.controller';
import { ISignupService } from '../service/isignup.service';
import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { CreateUser, CreateUserDto } from '../../../../domain/dtos/create.user';
import {
  BadRequestException,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
} from '@nestjs/common';
import { PreUserLoginCredential } from 'src/pre.user/data/pre.user.model';
import { IAuthService } from 'src/auth/iauth.service';
import { AuthServiceStub } from '../../auth/iauth.service.stub';
import {
  makeCreateUser,
  makeCreateUserDto,
} from '../../../../domain/dtos/create.user.factory';
import {
  makePreUser,
  makePreUserLoginCredential,
} from '../../pre.user/data/pre.user.model.factory';
import { SignupServiceStub } from '../service/isignup.service.stub';

interface SutComponents {
  signupService: ISignupService;
  authService: IAuthService;
}

const makeThrow = (
  components: SutComponents,
  component: string,
  method: string,
): Error => {
  const error = new Error(`Error with signupService ${method} method`);
  jest
    .spyOn(components[component], method)
    .mockImplementationOnce(async (...args: any[]): Promise<any> => {
      throw error;
    });
  return error;
};

describe('SignupController', () => {
  let sut: SignupController;
  let components: SutComponents;
  let email: string;
  let id: string;
  let createUserDto: CreateUserDto;
  let createUser: CreateUser;
  let preUserCredential: PreUserLoginCredential;

  beforeEach(async () => {
    email = faker.internet.email();
    id = faker.database.mongodbObjectId();
    createUserDto = makeCreateUserDto();
    createUser = makeCreateUser();
    preUserCredential = makePreUserLoginCredential();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SignupController],
      providers: [
        {
          provide: 'ISignupService',
          useClass: SignupServiceStub,
        },
        {
          provide: 'IAuthService',
          useClass: AuthServiceStub,
        },
      ],
    }).compile();

    sut = module.get<SignupController>(SignupController);
    components = {
      signupService: module.get<SignupServiceStub>('ISignupService'),
      authService: module.get<IAuthService>('IAuthService'),
    };
  });

  describe('pre user signup', () => {
    it('should call signup service with correct value', async () => {
      const checkUserExistsSpy = jest.spyOn(
        components.signupService,
        'checkUserExists',
      );
      await sut.signup(email);
      expect(checkUserExistsSpy).toHaveBeenCalledWith(email);
    });

    it('should throw a HttpException if user exists', async () => {
      jest
        .spyOn(components.signupService, 'checkUserExists')
        .mockReturnValueOnce(new Promise((resolve) => resolve(true)));
      try {
        await sut.signup(email);
        fail("this wasn't suposed to happen");
      } catch (error) {
        expect((error as HttpException).getResponse()).toEqual({
          status: 'failure',
          reason: 'user_exists',
        });
        expect((error as HttpException).getStatus()).toEqual(
          HttpStatus.BAD_REQUEST,
        );
      }
    });

    it('should return failure status if signupService checkUserExists method throws', async () => {
      const thrownError = makeThrow(
        components,
        'signupService',
        'checkUserExists',
      );
      let receivedError: InternalServerErrorException;
      try {
        await sut.signup(email);
      } catch (error) {
        receivedError = error;
      }
      expect(receivedError instanceof InternalServerErrorException).toEqual(
        true,
      );
      expect(receivedError.getResponse()).toEqual({
        status: 'failure',
        reason: 'component_failure',
        stack: thrownError,
      });
    });

    it('should return a pre user id on success', async () => {
      const response = await sut.signup(email);
      expect(response).toEqual({
        status: 'ok',
        payload: {
          preUserId: 'created_pre_user_id',
        },
      });
    });
  });

  describe('re send token', () => {
    it('should call signup service with correct value', async () => {
      const resendTokenSpy = jest.spyOn(
        components.signupService,
        'resendToken',
      );
      await sut.resendToken(id);
      expect(resendTokenSpy).toHaveBeenCalledWith(id);
    });

    it('should return failure status if signupService resendToken method throws', async () => {
      const thrownError = makeThrow(components, 'signupService', 'resendToken');
      let receivedError: InternalServerErrorException;
      try {
        await sut.resendToken(id);
      } catch (error) {
        receivedError = error;
      }
      expect(receivedError instanceof InternalServerErrorException).toEqual(
        true,
      );
      expect(receivedError.getResponse()).toEqual({
        status: 'failure',
        reason: 'component_failure',
        stack: thrownError,
      });
    });

    it('should return ok on success', async () => {
      const response = await sut.resendToken(id);
      expect(response).toEqual({
        status: 'ok',
      });
    });
  });

  describe('verify token', () => {
    it('should call auth service with correct value', async () => {
      const loginSpy = jest.spyOn(components.authService, 'login');
      await sut.verifyToken(preUserCredential);
      expect(loginSpy).toHaveBeenCalledWith(preUserCredential);
    });

    it('should return failure status if auth service login method throws', async () => {
      const thrownError = makeThrow(components, 'authService', 'login');
      let receivedError: InternalServerErrorException;
      try {
        await sut.verifyToken(preUserCredential);
      } catch (error) {
        receivedError = error;
      }
      expect(receivedError instanceof InternalServerErrorException).toEqual(
        true,
      );
      expect(receivedError.getResponse()).toEqual({
        status: 'failure',
        reason: 'component_failure',
        stack: thrownError,
      });
    });

    it('should return an access token on success', async () => {
      const response = await sut.verifyToken({
        id: 'valid_id',
        token: 'valid_token',
      });
      expect(response).toEqual({ accessToken: 'valid_token' });
    });
  });

  describe('create user', () => {
    it('should call signup service create user method with correct value', async () => {
      const createUserSpy = jest.spyOn(components.signupService, 'createUser');
      jest.spyOn(components.signupService, 'getPreUser').mockResolvedValueOnce({
        ...makePreUser(),
        email: createUser.email,
      });
      await sut.createUser({
        preUserId: preUserCredential.id,
        user: {
          password: createUser.password,
          passwordConfirmation: createUser.passwordConfirmation,
        },
      });
      expect(createUserSpy).toHaveBeenCalledWith(createUser);
    });

    it('should return failure status if signupService create user method throws', async () => {
      const thrownError = makeThrow(components, 'signupService', 'createUser');
      let receivedError: InternalServerErrorException;
      try {
        await sut.createUser({
          preUserId: preUserCredential.id,
          user: createUserDto,
        });
      } catch (error) {
        receivedError = error;
      }
      expect(receivedError instanceof InternalServerErrorException).toEqual(
        true,
      );
      expect(receivedError.getResponse()).toEqual({
        status: 'failure',
        reason: 'component_failure',
        stack: thrownError,
      });
    });

    it('should return failure status if user exists', async () => {
      jest.spyOn(components.signupService, 'createUser').mockResolvedValueOnce({
        status: 'failure',
        reason: 'user_exists',
      });
      let receivedError: HttpException;
      try {
        await sut.createUser({
          preUserId: preUserCredential.id,
          user: createUserDto,
        });
      } catch (error) {
        receivedError = error;
      }
      expect(receivedError instanceof BadRequestException).toEqual(true);
      expect(receivedError.getResponse()).toEqual({
        status: 'failure',
        reason: 'user_exists',
      });
    });

    it('should return an access token on success', async () => {
      const response = await sut.createUser({
        preUserId: preUserCredential.id,
        user: createUserDto,
      });
      expect(response).toEqual({
        status: 'ok',
        payload: { accessToken: 'valid_token' },
      });
    });
  });
});
