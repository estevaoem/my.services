import {
  BadRequestException,
  Body,
  Controller,
  HttpException,
  Inject,
  InternalServerErrorException,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { OperationStatus } from '../../../../domain/dtos/operation';
import { ISignupService } from '../service/isignup.service';
import { CreateUserDto } from '../../../../domain/dtos/create.user';
import { LocalAuthGuard } from '../../auth/guards/local-auth.guard';
import { PreUserLoginCredential } from 'src/pre.user/data/pre.user.model';
import { IAuthService } from 'src/auth/iauth.service';
import { AccessTokenDto } from '../../../../domain/dtos/access.token';
import { JWTAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { Id } from '../../../../domain/types';

@Controller('v1/signup')
export class SignupController {
  constructor(
    @Inject('ISignupService') private readonly signupService: ISignupService,
    @Inject('IAuthService') private readonly authService: IAuthService,
  ) {}

  @Post('pre-user')
  async signup(@Query('email') email: string): Promise<OperationStatus> {
    try {
      const userExists = await this.signupService.checkUserExists(email);
      if (userExists) {
        throw new BadRequestException({
          status: 'failure',
          reason: 'user_exists',
        });
      }

      const preUserId = await this.signupService.preRegisterUser(email);

      return {
        status: 'ok',
        payload: { preUserId: preUserId.toString() },
      };
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      }
      throw new InternalServerErrorException({
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      });
    }
  }

  @Post('token/resend')
  async resendToken(
    @Query('preUserId') preUserId: string,
  ): Promise<OperationStatus> {
    try {
      await this.signupService.resendToken(preUserId);

      return {
        status: 'ok',
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      });
    }
  }

  @UseGuards(LocalAuthGuard)
  @Post('token/verify')
  async verifyToken(
    @Body() body: PreUserLoginCredential,
  ): Promise<AccessTokenDto> {
    try {
      return await this.authService.login(body);
    } catch (error) {
      throw new InternalServerErrorException({
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      });
    }
  }

  @UseGuards(JWTAuthGuard)
  @Post('user')
  async createUser(
    @Body() body: { preUserId: Id; user: CreateUserDto },
  ): Promise<OperationStatus> {
    try {
      const preUser = await this.signupService.getPreUser({
        id: body.preUserId,
      });

      if (!preUser) {
        return {
          status: 'failure',
          reason: 'invalid_user',
        };
      }

      const createUserResult = await this.signupService.createUser({
        ...body.user,
        email: preUser.email,
      });

      if (createUserResult.reason === 'user_exists') {
        throw new BadRequestException(createUserResult);
      }

      const accessToken = await this.authService.login({
        id: createUserResult.payload.createdUserId,
      });
      return {
        status: 'ok',
        payload: accessToken,
      };
    } catch (error) {
      if (error instanceof HttpException) {
        throw error;
      }
      throw new InternalServerErrorException({
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      });
    }
  }
}
