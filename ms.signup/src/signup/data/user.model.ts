import { AvailablePlans, Id } from '../../../../domain/types';

export interface UserModel {
  id: Id;
  email: string;
  password: string;
  plan: AvailablePlans;
}
