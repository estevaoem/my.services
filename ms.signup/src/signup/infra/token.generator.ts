import { Injectable } from '@nestjs/common';
import { ITokenGenerator } from './itoken.generator';

@Injectable()
export class TokenGenerator implements ITokenGenerator {
  generate(size: number): string {
    if (size <= 0) {
      throw new RangeError(
        `Provided size ${size} for the token must be greater than 0`,
      );
    }
    const random = (Math.random() * 1000)
      .toPrecision(size)
      .toString()
      .replace('.', '');
    return random;
  }
}
