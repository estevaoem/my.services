/* eslint-disable @typescript-eslint/no-unused-vars */
import { faker } from '@faker-js/faker';
import { ITokenGenerator } from './itoken.generator';

export class TokenGeneratorStub implements ITokenGenerator {
  generate(size: number): string {
    return faker.datatype.number({ min: 1000, max: 9999 }).toString();
  }
}
