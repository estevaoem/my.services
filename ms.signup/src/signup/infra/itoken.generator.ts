export interface ITokenGenerator {
  generate(size: number): string;
}
