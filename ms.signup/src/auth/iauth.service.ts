import { AccessTokenDto } from '../../../domain/dtos/access.token';
import { PayloadWithId } from '../../../domain/types';

export interface IAuthService {
  validateUser(email: string, token: string): Promise<boolean>;
  login(credential: PayloadWithId): Promise<AccessTokenDto>;
}
