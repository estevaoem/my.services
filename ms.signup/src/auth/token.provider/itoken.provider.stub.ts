/* eslint-disable @typescript-eslint/no-unused-vars */
import { ITokenProvider } from './itoken.provider';

export class TokenProviderStub implements ITokenProvider {
  genToken(data: any): string {
    return 'generated_token';
  }
}
