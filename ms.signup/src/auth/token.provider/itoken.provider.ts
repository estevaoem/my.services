export interface ITokenProvider {
  genToken(data: any): string;
}
