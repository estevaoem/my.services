import { ITokenProvider } from './itoken.provider';
import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JWTTokenProvider implements ITokenProvider {
  constructor(private readonly jwtService: JwtService) {}
  genToken(data: any): string {
    return this.jwtService.sign(data);
  }
}
