/* eslint-disable @typescript-eslint/no-unused-vars */
import { AccessTokenDto } from '../../../domain/dtos/access.token';
import { PreUserLoginCredential } from 'src/pre.user/data/pre.user.model';
import { IAuthService } from './iauth.service';

export class AuthServiceStub implements IAuthService {
  async login(
    preUserCredential: PreUserLoginCredential,
  ): Promise<AccessTokenDto> {
    return {
      accessToken: 'valid_token',
    };
  }

  async validateUser(email: string, token: string): Promise<boolean> {
    return true;
  }
}
