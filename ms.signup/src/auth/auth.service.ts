import { Inject, Injectable } from '@nestjs/common';
import { AccessTokenDto } from '../../../domain/dtos/access.token';
import { environment } from '../common/environment';
import { IPreUserService } from 'src/pre.user/ipre.user.service';
import { IAuthService } from './iauth.service';
import { ITokenProvider } from './token.provider/itoken.provider';
import { Id, PayloadWithId } from '../../../domain/types';

@Injectable()
export class AuthService implements IAuthService {
  constructor(
    @Inject('IPreUserService') private preUserService: IPreUserService,
    @Inject('ITokenProvider') private tokenProvider: ITokenProvider,
  ) {}

  async validateUser(id: Id, token: string): Promise<any> {
    const preUser = {
      id,
      token,
    };
    const user = await this.preUserService.getPreUser(preUser);

    if (!user) {
      return null;
    }

    if (!user.validToken) {
      return null;
    }

    const now = new Date();
    const timeDeltaMs = Number(now) - Number(user.createdAt);
    const timeDeltaMin = timeDeltaMs / 1000 / 60;
    if (timeDeltaMin >= environment.TOKEN_EXPIRE_MIN) {
      return null;
    }

    await this.preUserService.invalidateToken(preUser.id);
    return preUser;
  }
  async login(credential: PayloadWithId): Promise<AccessTokenDto> {
    const accessToken = this.tokenProvider.genToken({
      id: credential.id,
    });
    return {
      accessToken,
    };
  }
}
