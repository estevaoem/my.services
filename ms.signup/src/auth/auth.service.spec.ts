/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { environment } from '../common/environment';
import {
  PreUser,
  PreUserLoginCredential,
} from 'src/pre.user/data/pre.user.model';
import { IPreUserService } from 'src/pre.user/ipre.user.service';
import { AuthService } from './auth.service';
import { ITokenProvider } from './token.provider/itoken.provider';
import { TokenProviderStub } from './token.provider/itoken.provider.stub';
import { PreUserServiceStub } from '../pre.user/ipre.user.service.stub';
import {
  makePreUser,
  makePreUserLoginCredential,
} from '../pre.user/data/pre.user.model.factory';

interface SutComponents {
  tokenProvider: ITokenProvider;
  preUserService: IPreUserService;
}

describe('AuthService', () => {
  let sut: AuthService;
  let components: SutComponents;
  let preUser: Omit<PreUser, 'validToken'>;
  let preUserLoginCredential: PreUserLoginCredential;

  beforeEach(async () => {
    preUserLoginCredential = makePreUserLoginCredential();
    preUser = { ...makePreUser(), ...preUserLoginCredential };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: 'IPreUserService',
          useClass: PreUserServiceStub,
        },
        {
          provide: 'ITokenProvider',
          useClass: TokenProviderStub,
        },
      ],
    }).compile();

    sut = module.get<AuthService>(AuthService);
    components = {
      preUserService: module.get<IPreUserService>('IPreUserService'),
      tokenProvider: module.get<ITokenProvider>('ITokenProvider'),
    };
  });

  describe('validate user', () => {
    it('should call pre user service with correct value', async () => {
      const getPreUserSpy = jest.spyOn(components.preUserService, 'getPreUser');
      await sut.validateUser(preUser.id, preUser.token);
      expect(getPreUserSpy).toHaveBeenCalledWith(preUserLoginCredential);
    });

    it('should return false if pre user service getUser method finds no user', async () => {
      jest
        .spyOn(components.preUserService, 'getPreUser')
        .mockReturnValueOnce(null);
      const response = await sut.validateUser(preUser.email, preUser.token);

      expect(response).toEqual(null);
    });

    it('should return false if token is invalid', async () => {
      jest
        .spyOn(components.preUserService, 'getPreUser')
        .mockReturnValueOnce(
          new Promise((resolve) =>
            resolve({ ...makePreUser(), validToken: false }),
          ),
        );
      const response = await sut.validateUser(preUser.email, preUser.token);

      expect(response).toEqual(null);
    });

    it('should return false if token expired', async () => {
      jest.spyOn(components.preUserService, 'getPreUser').mockReturnValueOnce(
        new Promise((resolve) =>
          resolve({
            ...makePreUser(),
            createdAt: new Date(
              Number(new Date()) - environment.TOKEN_EXPIRE_MIN * 60 * 1000,
            ),
          }),
        ),
      );
      const response = await sut.validateUser(preUser.email, preUser.token);

      expect(response).toEqual(null);
    });

    it('should call pre user service invalidateToken method with correct value', async () => {
      const invalidateTokenSpy = jest.spyOn(
        components.preUserService,
        'invalidateToken',
      );
      await sut.validateUser(preUser.id, preUser.token);
      expect(invalidateTokenSpy).toHaveBeenCalledWith(
        preUserLoginCredential.id,
      );
    });

    it('should return the pre user on success', async () => {
      const response = await sut.validateUser(
        preUserLoginCredential.id,
        preUserLoginCredential.token,
      );
      expect(response).toEqual(preUserLoginCredential);
    });
  });

  describe('login', () => {
    it('should call token provider with correct value', async () => {
      const genTokenSpy = jest.spyOn(components.tokenProvider, 'genToken');
      await sut.login(preUserLoginCredential);
      expect(genTokenSpy).toHaveBeenCalledWith({
        id: preUserLoginCredential.id,
      });
    });

    it('should throw if token provider throws', async () => {
      jest
        .spyOn(components.tokenProvider, 'genToken')
        .mockImplementationOnce((data: any): string => {
          throw new Error('Error with genToken method');
        });
      const result = sut.login(preUserLoginCredential);
      expect(result).rejects.toThrow();
    });

    it('should return generated access token on success', async () => {
      const response = await sut.login(preUserLoginCredential);
      expect(response).toEqual({ accessToken: 'generated_token' });
    });
  });
});
