import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './guards/local.strategy';
import { PreUserModule } from '../pre.user/pre.user.module';
import { JwtModule } from '@nestjs/jwt';
import { environment } from '../common/environment';
import { JWTTokenProvider } from './token.provider/jwt.token.provider';
import { JwtStrategy } from './guards/jwt.strategy';

@Module({
  imports: [
    PreUserModule,
    PassportModule,
    JwtModule.register({
      secret: environment.AUTH_TOKEN_SECRET,
      signOptions: { expiresIn: `${environment.AUTH_TOKEN_EXPIRATION_MIN}m` },
    }),
  ],
  providers: [
    {
      provide: 'IAuthService',
      useClass: AuthService,
    },
    LocalStrategy,
    JwtStrategy,
    {
      provide: 'ITokenProvider',
      useClass: JWTTokenProvider,
    },
  ],
  exports: [
    {
      provide: 'IAuthService',
      useClass: AuthService,
    },
  ],
})
export class AuthModule {}
