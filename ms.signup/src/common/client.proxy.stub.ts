/* eslint-disable @typescript-eslint/no-unused-vars */
import { ClientProxy, ReadPacket, WritePacket } from '@nestjs/microservices';
import { Observable } from 'rxjs';

export class ClientProxyStub extends ClientProxy {
  connect(): Promise<any> {
    throw new Error('Method not implemented.');
  }
  close() {
    throw new Error('Method not implemented.');
  }
  protected publish(
    packet: ReadPacket<any>,
    callback: (packet: WritePacket<any>) => void,
  ): () => void {
    throw new Error('Method not implemented.');
  }
  protected dispatchEvent<T = any>(packet: ReadPacket<any>): Promise<T> {
    throw new Error('Method not implemented.');
  }
  send<TResult = any, TInput = any>(
    pattern: any,
    data: TInput,
  ): Observable<TResult> {
    return this as unknown as Observable<TResult>;
  }

  pipe(...args: any[]): ClientProxy {
    return this;
  }

  public async toPromise(): Promise<object> {
    return {
      defined: 'object',
    };
  }

  emit<TResult = any, TInput = any>(
    pattern: any,
    data: TInput,
  ): Observable<TResult> {
    return this as unknown as Observable<TResult>;
  }
}
