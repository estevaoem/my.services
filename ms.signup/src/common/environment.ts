export const environment = {
  DB_USER: process.env.DB_USER ?? 'admin',
  DB_PASSWORD: process.env.DB_PASSWORD ?? 'admin',
  DB_HOST: process.env.DB_HOST ?? 'signup_ms_db',
  DB_PORT: process.env.DB_PORT ?? 27017,
  DB_NAME: process.env.DB_NAME ?? 'signup',
  MESSAGE_BROKER_HOST: process.env.MESSAGE_BROKER_HOST ?? 'signup_ms_messaging',
  MESSAGE_BROKER_PORT: process.env.MESSAGE_BROKER_PORT ?? 5672,
  MESSAGE_BROKER_USER: process.env.MESSAGE_BROKER_USER ?? 'guest',
  MESSAGE_BROKER_PASSWORD: process.env.MESSAGE_BROKER_PASSWORD ?? 'guest',
  AUTH_TOKEN_SECRET: process.env.AUTH_TOKEN_SECRET ?? 'superSecret',
  AUTH_TOKEN_EXPIRATION_MIN: process.env.AUTH_TOKEN_EXPIRATION_MIN
    ? Number(process.env.AUTH_TOKEN_EXPIRATION_MIN)
    : 20,
  TOKEN_SIZE: process.env.TOKEN_SIZE ? Number(process.env.TOKEN_SIZE) : 4,
  TOKEN_EXPIRE_MIN: process.env.TOKEN_EXPIRE_MIN
    ? Number(process.env.TOKEN_EXPIRE_MIN)
    : 3,
  SERVICE_RESPONSE_TIMEOUT: process.env.SERVICE_RESPONSE_TIMEOUT
    ? Number(process.env.SERVICE_RESPONSE_TIMEOUT)
    : 5000,
  SERVER_PORT: process.env.SERVER_PORT ? Number(process.env.SERVER_PORT) : 3000,
  // origins separated by ;
  CORS_ALLOWED_ORIGINS:
    process.env.CORS_ALLOWED_ORIGINS ?? '.*;http://127.0.0.1:8000',
};
