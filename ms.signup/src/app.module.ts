import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { environment } from './common/environment';
import { SignupModule } from './signup/signup.module';
import { AuthModule } from './auth/auth.module';
import { PreUserModule } from './pre.user/pre.user.module';
import { APP_GUARD } from '@nestjs/core';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';

const mongoUri = `mongodb://${environment.DB_HOST}:${environment.DB_PORT}`;

@Module({
  imports: [
    MongooseModule.forRoot(mongoUri, {
      auth: {
        username: environment.DB_USER,
        password: environment.DB_PASSWORD,
      },
      // authSource: 'admin',
      dbName: environment.DB_NAME,
    }),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
    SignupModule,
    AuthModule,
    PreUserModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
})
export class AppModule {}
