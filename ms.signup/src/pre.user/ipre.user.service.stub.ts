/* eslint-disable @typescript-eslint/no-unused-vars */
import { Id } from '../../../domain/types';
import {
  PreUserCredential,
  PreUser,
  PreUserLoginCredential,
} from './data/pre.user.model';
import { makePreUser } from './data/pre.user.model.factory';
import { IPreUserService } from './ipre.user.service';

export class PreUserServiceStub implements IPreUserService {
  async getPreUser(preUser: Partial<PreUser>): Promise<PreUser> {
    const madePreUser = makePreUser();
    return {
      ...madePreUser,
      ...preUser,
    };
  }
  async preRegisterUser(credential: PreUserCredential): Promise<Id> {
    return 'pre_user_id';
  }

  async invalidateToken(credential: Id): Promise<void> {
    return;
  }
}
