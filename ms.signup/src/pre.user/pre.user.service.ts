import { Inject, Injectable } from '@nestjs/common';
import { PreUser, PreUserCredential } from './data/pre.user.model';
import { IPreUserRepository } from './data/ipre.user.repository';
import { IPreUserService } from './ipre.user.service';
import { Id } from '../../../domain/types';

@Injectable()
export class PreUserService implements IPreUserService {
  constructor(
    @Inject('IPreUserRepository')
    private readonly preUserRepository: IPreUserRepository,
  ) {}

  async preRegisterUser(credential: PreUserCredential): Promise<Id> {
    return await this.preUserRepository.createPreUser({
      ...credential,
      validToken: true,
      createdAt: new Date(),
    });
  }

  async getPreUser(preUser: Partial<PreUser>): Promise<PreUser> {
    const storedPreUser = await this.preUserRepository.getPreUser(preUser);
    if (!storedPreUser) {
      return null;
    }
    return storedPreUser;
  }

  async invalidateToken(preUserId: Id): Promise<void> {
    await this.preUserRepository.updatePreUser(
      { id: preUserId },
      {
        validToken: false,
      },
    );
  }
}
