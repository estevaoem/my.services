import { Id } from '../../../domain/types';
import { PreUser, PreUserCredential } from './data/pre.user.model';

export interface IPreUserService {
  preRegisterUser(credential: PreUserCredential): Promise<Id>;
  getPreUser(preUser: Partial<PreUser>): Promise<PreUser | null>;
  invalidateToken(credential: Id): Promise<void>;
}
