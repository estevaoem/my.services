/* eslint-disable @typescript-eslint/no-unused-vars */
import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { Id } from '../../../domain/types';
import { IPreUserRepository } from './data/ipre.user.repository';
import { PreUserRepositoryStub } from './data/ipre.user.repository.stub';
import {
  PreUserCredential,
  PreUserLoginCredential,
} from './data/pre.user.model';
import {
  makePreUserCredential,
  makePreUserLoginCredential,
} from './data/pre.user.model.factory';
import { PreUserService } from './pre.user.service';

const makeThrow = (
  components: SutComponents,
  component: string,
  method: string,
): void => {
  jest
    .spyOn(components[component], method)
    .mockImplementationOnce(async (...args: any[]): Promise<any> => {
      throw new Error(`Error with signupService ${method} method`);
    });
};

interface SutComponents {
  preUserRepository: IPreUserRepository;
}

describe('PreUserService', () => {
  let sut: PreUserService;
  let components: SutComponents;
  let preUserCredential: PreUserCredential;
  let preUserLoginCredential: PreUserLoginCredential;
  let id: Id;

  beforeEach(async () => {
    preUserCredential = makePreUserCredential();
    preUserLoginCredential = makePreUserLoginCredential();
    id = faker.database.mongodbObjectId();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PreUserService,
        { provide: 'IPreUserRepository', useClass: PreUserRepositoryStub },
      ],
    }).compile();

    sut = module.get<PreUserService>(PreUserService);
    components = {
      preUserRepository: module.get<IPreUserRepository>('IPreUserRepository'),
    };
  });

  describe('pre register user', () => {
    it('should call pre user repository with correct values', async () => {
      const createPreUserSpy = jest.spyOn(
        components.preUserRepository,
        'createPreUser',
      );

      await sut.preRegisterUser(preUserCredential);
      const callStack = createPreUserSpy.mock.calls[0][0];
      const { createdAt, ...rest } = callStack;
      expect(createdAt).toBeDefined();
      expect(rest).toEqual({ ...preUserCredential, validToken: true });
    });

    it('should throw if pre user repository throws', async () => {
      makeThrow(components, 'preUserRepository', 'createPreUser');
      const preUserCreationResult = sut.preRegisterUser(preUserCredential);
      expect(preUserCreationResult).rejects.toThrow();
    });

    it('should return a pre user id on success', async () => {
      const preUserId = await sut.preRegisterUser(preUserCredential);
      expect(preUserId).toEqual('created_pre_user_id');
    });
  });

  describe('get pre user', () => {
    it('should call pre user repository with correct values', async () => {
      const getPreUserSpy = jest.spyOn(
        components.preUserRepository,
        'getPreUser',
      );

      await sut.getPreUser({ id });
      expect(getPreUserSpy).toHaveBeenCalledWith({ id });
    });

    it('should throw if pre user repository throws', async () => {
      makeThrow(components, 'preUserRepository', 'getPreUser');
      const preUserCreationResult = sut.getPreUser({ id });
      expect(preUserCreationResult).rejects.toThrow();
    });

    it('should return null if no pre user is found', async () => {
      jest
        .spyOn(components.preUserRepository, 'getPreUser')
        .mockReturnValueOnce(null);
      const storedPreUser = await sut.getPreUser({ id });
      expect(storedPreUser).toEqual(null);
    });

    it('should return stored pre user on success', async () => {
      const storedPreUser = await sut.getPreUser({ id });
      expect(storedPreUser).toEqual(PreUserRepositoryStub.storedPreUser);
    });
  });

  describe('invalidate token', () => {
    it('should call pre user repository with correct values', async () => {
      const updatePreUserSpy = jest.spyOn(
        components.preUserRepository,
        'updatePreUser',
      );

      await sut.invalidateToken(preUserLoginCredential.id);
      expect(updatePreUserSpy).toHaveBeenCalledWith(
        { id: preUserLoginCredential.id },
        {
          validToken: false,
        },
      );
    });

    it('should throw if pre user repository throws', async () => {
      makeThrow(components, 'preUserRepository', 'updatePreUser');
      const preUserCreationResult = sut.invalidateToken(
        preUserLoginCredential.id,
      );
      expect(preUserCreationResult).rejects.toThrow();
    });
  });
});
