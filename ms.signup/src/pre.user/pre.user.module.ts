import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PreUserSchema } from './data/mongo/pre.user.model.mongodb';
import { MongoPreUserRepository } from './data/mongo/pre.user.repository';
import { PreUserService } from './pre.user.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'PreUser',
        schema: PreUserSchema,
        collection: 'preuser',
      },
    ]),
  ],
  providers: [
    {
      provide: 'IPreUserService',
      useClass: PreUserService,
    },
    {
      provide: 'IPreUserRepository',
      useClass: MongoPreUserRepository,
    },
  ],
  exports: [
    {
      provide: 'IPreUserService',
      useClass: PreUserService,
    },
  ],
})
export class PreUserModule {}
