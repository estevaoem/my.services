import { Id } from '../../../../domain/types';

export interface PreUserCredential {
  email: string;
  token: string;
}

export interface PreUserLoginCredential {
  id: Id;
  token: string;
}

export interface PreUser extends PreUserCredential, PreUserLoginCredential {
  validToken: boolean;
  createdAt: Date;
}
