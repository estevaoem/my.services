import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Id } from '../../../../../domain/types';
import { IPreUserRepository } from '../ipre.user.repository';
import { PreUser } from '../pre.user.model';
import { PreUserDocument } from './pre.user.model.mongodb';

@Injectable()
export class MongoPreUserRepository implements IPreUserRepository {
  constructor(
    @InjectModel('PreUser')
    private readonly signupModel: Model<PreUserDocument>,
  ) {}
  async createPreUser(preUser: PreUser): Promise<Id> {
    if (Object.keys(preUser).includes('id')) {
      delete preUser.id;
    }
    const response = await this.signupModel.insertMany([preUser]);
    return response[0]._id.toString();
  }

  async getPreUser(preUser: Partial<PreUser>): Promise<PreUser> {
    if (Object.keys(preUser).includes('id')) {
      (preUser as any)._id = preUser.id;
      delete preUser.id;
    }
    const found = await this.signupModel
      .find(preUser)
      .sort({ createdAt: 'ascending' })
      .limit(1);

    if (found[0] === undefined) {
      return null;
    }

    return found[0] as unknown as PreUser;
  }

  async updatePreUser(
    preUser: Partial<PreUser>,
    updates: Partial<PreUser>,
  ): Promise<void> {
    if (Object.keys(preUser).includes('id')) {
      (preUser as any)._id = preUser.id;
      delete preUser.id;
    }
    await this.signupModel.updateMany({
      ...preUser,
      $set: updates,
    });
  }
}
