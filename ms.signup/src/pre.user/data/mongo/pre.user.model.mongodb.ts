import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { PreUser } from '../pre.user.model';

@Schema()
export class MongoDbPreUser implements PreUser {
  @Prop({ type: mongoose.Schema.Types.ObjectId })
  id: string;

  @Prop({ type: String, required: true })
  email: string;

  @Prop({ type: String, required: true })
  token: string;

  @Prop({ type: Boolean, required: true })
  validToken: boolean;

  @Prop({ type: Date, required: true })
  createdAt: Date;
}

export type PreUserDocument = HydratedDocument<MongoDbPreUser>;
export const PreUserSchema = SchemaFactory.createForClass(MongoDbPreUser);
