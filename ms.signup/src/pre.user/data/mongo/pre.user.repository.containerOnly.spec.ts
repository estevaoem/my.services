/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { PreUser } from '../pre.user.model';
import { MongoPreUserRepository } from './pre.user.repository';
import { makePreUser } from '../pre.user.model.factory';
import { MongoHelper } from './mongo.helper.factory';

const mongoHelper = new MongoHelper();

describe('MongoPreUserRepository', () => {
  let sut: MongoPreUserRepository;
  let preUser: PreUser;

  beforeAll(async () => {
    await mongoHelper.setup();
  });

  afterAll(async () => {
    await mongoHelper.clearDb();
  });

  beforeEach(async () => {
    await mongoHelper.clearDb();

    preUser = makePreUser();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MongoPreUserRepository,
        {
          provide: getModelToken('PreUser'),
          useValue: mongoHelper.preUsersModel,
        },
      ],
    }).compile();

    sut = module.get<MongoPreUserRepository>(MongoPreUserRepository);
  });

  describe('create pre user', () => {
    it('should call insertMany with correct value', async () => {
      const insertManySpy = jest.spyOn(mongoHelper.preUsersModel, 'insertMany');
      await sut.createPreUser(preUser);
      const callStack = insertManySpy.mock.calls[0][0][0];
      const { createdAt, ...rest } = callStack;
      expect(createdAt).toBeDefined();
      expect(rest).toEqual({
        email: preUser.email,
        token: preUser.token,
        validToken: preUser.validToken,
      });
    });

    it('should throw if insertMany throws', async () => {
      jest
        .spyOn(mongoHelper.preUsersModel, 'insertMany')
        .mockImplementationOnce(() => {
          throw new Error('Error on insertMany');
        });
      const userCreation = sut.createPreUser(preUser);
      expect(userCreation).rejects.toThrow();
    });

    it('should create a pre user on success', async () => {
      await sut.createPreUser(preUser);
      expect((await mongoHelper.getPreUsers()).length).toEqual(1);
    });

    it('should return a pre user id on success', async () => {
      const id = await sut.createPreUser(preUser);
      expect(
        ((await mongoHelper.getPreUsers())[0] as any)._id.toString(),
      ).toEqual(id);
    });
  });

  describe('get pre user', () => {
    it('should call findOne with correct value', async () => {
      const findSpy = jest.spyOn(mongoHelper.preUsersModel, 'find');
      await sut.getPreUser(preUser);
      expect(findSpy).toHaveBeenCalledWith(preUser);
    });

    it('should throw if find throws', async () => {
      jest
        .spyOn(mongoHelper.preUsersModel, 'find')
        .mockImplementationOnce(() => {
          throw new Error('Error on find');
        });
      const preUserGet = sut.getPreUser(preUser);
      expect(preUserGet).rejects.toThrow();
    });

    it('should return the last created pre user on multiple pre users', async () => {
      // first create a user and insert it to db
      await mongoHelper.insertPreUser(preUser);
      const storedPreUser = await mongoHelper.getPreUsers();

      // change created user time to now and insert it
      preUser.createdAt = new Date(Number(preUser.createdAt) - 3600);
      await mongoHelper.insertPreUser(preUser);

      const { createdAt, ...pUser } = preUser;

      const result = await sut.getPreUser(pUser);
      expect(result).not.toEqual(storedPreUser[0]);
    });

    it('should return null if no preUser is found', async () => {
      const pUser = await sut.getPreUser(preUser);
      expect(pUser).toBe(null);
    });

    it('should return a preUser on success', async () => {
      await mongoHelper.insertPreUser(preUser);
      const storedPreUsers = await mongoHelper.getPreUsers();

      const getPreUserResult = await sut.getPreUser({
        id: (storedPreUsers[0] as any)._id,
      });
      expect(getPreUserResult.email).toEqual(preUser.email);
    });
  });

  describe('update pre user', () => {
    it('should call updateOne with correct value', async () => {
      const updateOneSpy = jest.spyOn(mongoHelper.preUsersModel, 'updateMany');

      const { validToken, createdAt, ...rest } = preUser;

      await sut.updatePreUser(rest, { validToken: false });
      expect(updateOneSpy).toHaveBeenCalledWith({
        ...rest,
        $set: { validToken: false },
      });
    });

    it('should throw if updateOne throws', async () => {
      jest
        .spyOn(mongoHelper.preUsersModel, 'updateMany')
        .mockImplementationOnce(() => {
          throw new Error('Error on updateOne');
        });
      const { validToken, createdAt, ...rest } = preUser;

      const updateResult = sut.updatePreUser(rest, { validToken: false });
      expect(updateResult).rejects.toThrow();
    });

    it('should update an user on success', async () => {
      await mongoHelper.insertPreUser(preUser);
      const storedPreUsers = await mongoHelper.getPreUsers();

      await sut.updatePreUser(
        { id: (storedPreUsers[0] as any)._id },
        { validToken: false },
      );

      const updatedstoredUser = await mongoHelper.getPreUsers();
      expect(preUser.validToken).toEqual(true);
      expect((updatedstoredUser as PreUser[])[0].validToken).toEqual(false);
    });
  });
});
