/* eslint-disable @typescript-eslint/no-unused-vars */
import { connect, Connection, Model } from 'mongoose';
import { MongoDbPreUser, PreUserSchema } from './pre.user.model.mongodb';
import { environment } from '../../../common/environment';
import { PreUser } from '../pre.user.model';

const mongoUri = `mongodb://${environment.DB_USER}:${environment.DB_PASSWORD}@${environment.DB_HOST}:${environment.DB_PORT}/${environment.DB_NAME}?authSource=admin`;

export class MongoHelper {
  private mongoConnection: Connection;
  preUsersModel: Model<MongoDbPreUser>;

  async setup() {
    this.mongoConnection = (await connect(mongoUri)).connection;
    this.preUsersModel = this.mongoConnection.model('PreUser', PreUserSchema);
  }

  async clearDb(): Promise<void> {
    await this.preUsersModel.deleteMany({});
  }

  async getPreUsers(): Promise<PreUser[]> {
    return await this.preUsersModel.find();
  }

  async insertPreUser(preUser: PreUser): Promise<void> {
    await this.preUsersModel.insertMany([preUser]);
  }
}
