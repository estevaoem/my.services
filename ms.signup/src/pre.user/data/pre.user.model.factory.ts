import { faker } from '@faker-js/faker';
import {
  PreUser,
  PreUserCredential,
  PreUserLoginCredential,
} from './pre.user.model';

export const makePreUserCredential = (): PreUserCredential => ({
  email: faker.internet.email(),
  token: faker.datatype.number({ min: 1000, max: 9999 }).toString(),
});

export const makePreUserLoginCredential = (): PreUserLoginCredential => ({
  id: faker.database.mongodbObjectId(),
  token: faker.datatype.number({ min: 1000, max: 9999 }).toString(),
});

export const makePreUser = (): PreUser => {
  return {
    id: faker.database.mongodbObjectId(),
    ...makePreUserCredential(),
    validToken: true,
    createdAt: new Date(),
  };
};
