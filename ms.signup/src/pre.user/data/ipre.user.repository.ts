import { Id } from '../../../../domain/types';
import { PreUser } from './pre.user.model';

export interface IPreUserRepository {
  createPreUser(user: Omit<PreUser, 'id'>): Promise<Id>;
  getPreUser(preUser: Partial<PreUser>): Promise<PreUser | null>;
  updatePreUser(
    preUser: Partial<PreUser>,
    updates: Partial<PreUser>,
  ): Promise<void>;
}
