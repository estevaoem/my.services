/* eslint-disable @typescript-eslint/no-unused-vars */
import { Id } from '../../../../domain/types';
import { IPreUserRepository } from './ipre.user.repository';
import { PreUser, PreUserCredential } from './pre.user.model';
import { makePreUser } from './pre.user.model.factory';

export class PreUserRepositoryStub implements IPreUserRepository {
  static storedPreUser: PreUser = makePreUser();

  async getPreUser(preUser: Partial<PreUser>): Promise<PreUser> {
    PreUserRepositoryStub.storedPreUser = {
      ...PreUserRepositoryStub.storedPreUser,
      ...preUser,
    };
    return PreUserRepositoryStub.storedPreUser;
  }
  async updatePreUser(
    preUser: PreUserCredential,
    updates: Partial<PreUser>,
  ): Promise<void> {
    return;
  }
  async createPreUser(user: PreUser): Promise<Id> {
    return 'created_pre_user_id';
  }
}
