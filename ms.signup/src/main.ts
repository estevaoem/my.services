import { InternalServerErrorException } from '@nestjs/common';
import { CustomOrigin } from '@nestjs/common/interfaces/external/cors-options.interface';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import helmet from 'helmet';
import { AppModule } from './app.module';
import { environment } from './common/environment';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    transport: Transport.RMQ,
    options: {
      urls: [
        `amqp://${environment.MESSAGE_BROKER_HOST}:${environment.MESSAGE_BROKER_PORT}`,
      ],
      queue: 'signup_queue',
      queueOptions: {
        durable: false,
      },
    },
  });

  // Helmet
  app.use(helmet());

  // CORS
  // https://www.npmjs.com/package/cors#configuring-cors-w-dynamic-origin
  const allowedOrigins = environment.CORS_ALLOWED_ORIGINS.split(';');
  const customOrigin: CustomOrigin = (
    requestOrigin: string,
    callback: (err: Error | null, origin?: boolean) => void,
  ): void => {
    for (const allowedOrigin of allowedOrigins) {
      if (requestOrigin.match(allowedOrigin)) {
        callback(null, true);
        return;
      }
    }
    if (allowedOrigins.indexOf(requestOrigin) !== -1) {
      callback(null, true);
    } else {
      callback(
        new InternalServerErrorException(
          `Request origin ${requestOrigin} not allowed by CORS`,
        ),
      );
    }
  };

  app.enableCors({ origin: customOrigin });

  // Server Start
  await app.startAllMicroservices();
  await app.listen(environment.SERVER_PORT);
  console.log(`Server started at PORT ${environment.SERVER_PORT}`)
}
bootstrap();
