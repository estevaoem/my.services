import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { OperationStatus } from '../../../domain/dtos/operation';
import { UserModel } from 'src/signup/data/user.model';
import { faker } from '@faker-js/faker';

@Controller('messaging')
export class MessagingController {
  @MessagePattern({ cmd: 'check_user_exists' })
  async checkUserExistsByEmail(email: string): Promise<boolean> {
    const stubEmails = [
      'Rosalia_Beatty@gmail.com',
      'Odessa_Schmidt19@gmail.com',
      'Kristin3@gmail.com',
      'Cathryn.Lang28@gmail.com',
      'Lonny.Roberts@hotmail.com',
    ];
    if (stubEmails.includes(email)) {
      return true;
    }
    return false;
  }

  @MessagePattern({ cmd: 'create_user' })
  async createUser(user: Omit<UserModel, 'id'>): Promise<OperationStatus> {
    const failStubEmails = [
      'Helen.Ruecker@yahoo.com',
      'Americo_Keeling74@hotmail.com',
      'Enoch6@hotmail.com',
      'Tremayne.Rau74@yahoo.com',
      'Mayra1@gmail.com',
    ];
    if (failStubEmails.includes(user.email)) {
      return {
        status: 'failure',
        reason: 'user_exists',
      };
    }
    return {
      status: 'ok',
      payload: { createdUserId: faker.database.mongodbObjectId() },
    };
  }
}
