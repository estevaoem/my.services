import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request from 'supertest';
import { AppModule } from './../src/app.module';
import { MessagingModule } from './messaging/messaging.module';
import { Transport } from '@nestjs/microservices';
import { environment } from '../src/common/environment';
import { connect, Connection, Model } from 'mongoose';
import {
  MongoDbPreUser,
  PreUserSchema,
} from '../src/pre.user/data/mongo/pre.user.model.mongodb';
import { PreUser } from '../src/pre.user/data/pre.user.model';
import { faker } from '@faker-js/faker';
import { JwtModule } from '@nestjs/jwt';
import { JWTTokenProvider } from './../src/auth/token.provider/jwt.token.provider';
import { makePreUser } from './../src/pre.user/data/pre.user.model.factory';

const mongoUri = `mongodb://${environment.DB_USER}:${environment.DB_PASSWORD}@${environment.DB_HOST}:${environment.DB_PORT}/${environment.DB_NAME}?authSource=admin`;

class MongoHelper {
  mongoConnection: Connection;
  preUsersModel: Model<MongoDbPreUser>;

  async setup() {
    this.mongoConnection = (await connect(mongoUri)).connection;
    this.preUsersModel = this.mongoConnection.model(
      'PreUser',
      PreUserSchema,
      'preuser',
    );
  }

  async clearDb(): Promise<void> {
    await this.preUsersModel.deleteMany({});
  }

  async getPreUsers(): Promise<PreUser[]> {
    return await this.preUsersModel.find();
  }

  async insertPreUser(preUser: PreUser): Promise<void> {
    await this.preUsersModel.insertMany([preUser]);
  }
}

const mongoHelper = new MongoHelper();

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let userServiceAppStub: INestApplication;
  let jwt: string;
  let preUser: PreUser;

  beforeAll(async (): Promise<void> => {
    await mongoHelper.setup();
    await mongoHelper.clearDb();
  });

  beforeEach(async () => {
    preUser = makePreUser();
    await mongoHelper.insertPreUser(preUser);

    const holderFixture: TestingModule = await Test.createTestingModule({
      imports: [MessagingModule],
    }).compile();

    userServiceAppStub = holderFixture.createNestApplication();

    userServiceAppStub.connectMicroservice({
      transport: Transport.RMQ,
      options: {
        urls: [
          `amqp://${environment.MESSAGE_BROKER_HOST}:${environment.MESSAGE_BROKER_PORT}`,
        ],
        queue: 'user_creation_queue',
        queueOptions: {
          durable: false,
        },
      },
    });
    await userServiceAppStub.startAllMicroservices();
    await userServiceAppStub.init();

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        JwtModule.register({
          secret: environment.AUTH_TOKEN_SECRET,
          signOptions: {
            expiresIn: `${environment.AUTH_TOKEN_EXPIRATION_MIN}m`,
          },
        }),
      ],
      providers: [JWTTokenProvider],
    }).compile();

    const jwtTokenProvider =
      moduleFixture.get<JWTTokenProvider>(JWTTokenProvider);
    jwt = jwtTokenProvider.genToken({ email: 'e2e_user@gmail.com' });

    app = moduleFixture.createNestApplication();

    await app.startAllMicroservices();
    await app.init();
  });

  afterEach(async (): Promise<void> => {
    await mongoHelper.clearDb();
  });

  afterAll(async (): Promise<void> => {
    await mongoHelper.mongoConnection.close();
  });

  describe('/pre-user', () => {
    it('should create a user', async () => {
      await mongoHelper.clearDb();
      const email = `TEST.${faker.internet.email()}`;
      const response = await request(app.getHttpServer())
        .post('/v1/signup/pre-user')
        .query({ email });
      expect(response.statusCode).toEqual(201);
      expect((await mongoHelper.getPreUsers())[0].email).toEqual(email);
    });

    it('should fail if a user exists', async () => {
      const response = await request(app.getHttpServer())
        .post('/v1/signup/pre-user')
        .query({ email: 'Kristin3@gmail.com' });
      expect(response.statusCode).toEqual(400);
    });
  });

  describe('/token/resend', () => {
    it('should return ok on success', async () => {
      const email = `TEST.${faker.internet.email()}`;
      const response = await request(app.getHttpServer())
        .post('/v1/signup/token/resend')
        .query({ email });
      expect(response.statusCode).toEqual(201);
      expect(response.body).toEqual({
        status: 'ok',
      });
    });
  });

  describe('/token/verify', () => {
    it('should return a jwt token on success', async () => {
      const preUsers = await mongoHelper.getPreUsers();
      const pUser = preUsers[0];
      const response = await request(app.getHttpServer())
        .post('/v1/signup/token/verify')
        .send({ id: (pUser as any)._id, token: pUser.token });
      expect(response.statusCode).toEqual(201);
      expect(response.body.accessToken).toBeDefined();
    });
  });

  describe('/user', () => {
    it('should return unauthorized if no credentials are provided', async () => {
      const password = faker.internet.password();
      const response = await request(app.getHttpServer())
        .post('/v1/signup/user')
        .send({
          email: faker.internet.email(),
          password,
          passwordConfirmation: password,
        });
      expect(response.statusCode).toEqual(401);
    });

    it('should create a user on success', async () => {
      await mongoHelper.clearDb();
      await mongoHelper.insertPreUser(preUser);
      const storedPreUser = await mongoHelper.getPreUsers();
      const password = faker.internet.password();
      const response = await request(app.getHttpServer())
        .post('/v1/signup/user')
        .set('Authorization', `Bearer ${jwt}`)
        .send({
          preUserId: (storedPreUser[0] as any)._id,
          password,
          passwordConfirmation: password,
        });
      expect(response.body.status).toEqual('ok');
      expect(response.body.payload.accessToken).toBeDefined();
      expect(response.statusCode).toEqual(201);
    });
  });

  describe('Full signup', () => {
    it('should complete the whole signup process', async () => {
      await mongoHelper.clearDb();
      const email = `TEST.${faker.internet.email()}`;
      const preUserCreationResponse = await request(app.getHttpServer())
        .post('/v1/signup/pre-user')
        .query({ email });
      const preUserId = preUserCreationResponse.body.payload.preUserId;
      const token = (await mongoHelper.getPreUsers())[0].token;
      const loginResponse = await request(app.getHttpServer())
        .post('/v1/signup/token/verify')
        .send({ id: preUserId, token });
      const accessToken = loginResponse.body.accessToken;
      const password = faker.internet.password();
      const response = await request(app.getHttpServer())
        .post('/v1/signup/user')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          preUserId,
          password,
          passwordConfirmation: password,
        });
      expect(response.statusCode).toEqual(201);
      expect(response.body.status).toEqual('ok');
      expect(response.body.payload.accessToken).toBeDefined();
      expect(response.statusCode).toEqual(201);
    });
  });
});
