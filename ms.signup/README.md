# Microservices Demo - Signup Service

## Description

This service aims to handle signup related information, as well as operations regarding the signup process, for [this](../docs/my-services-signup-flow.png) microservices' schema.

## Features
- Headers setup with helmet
- Request throttling
- Dynamic CORS setup
- Db local authentication with passport
- Local authorization with JWT
- Password encryption
- Messaging request-response pattern
- Messaging event pattern
- 100% coverage for unit tests
- End to end integration test 

## Technologies
- NestJS
- Jest
- RabbitMQ
- MongoDb
- Docker

## Installation

```bash
$ npm install
```

## Running the app

```bash
# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

For tests that doesn't require external connections (i.e., things other than databases and messaging)
```bash
# unit tests
$ npm run test:watch

# test coverage
$ npm run test:cov
```

otherwise, raise the containers with `docker-compose up` and then
```bash
# unit tests
$ npm run test:containerWatch
# integration tests
$ npm run test:e2e
```
