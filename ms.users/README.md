# Microservices Demo - Users Service

## Description

This service aims to register user related information, as well as operations regarding users for [this](../docs/services_diagram.png) microservices schema.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

For tests that doesn't require external connections (i.e., things other than databases and messaging)
```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

otherwise, raise the containers with `docker-compose up` and then
```bash
# unit tests
$ docker exec -it users_ms npm run test:containerWatch
```