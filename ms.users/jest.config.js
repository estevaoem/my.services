module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '.+\\.ts$': 'ts-jest',
  },
  // transform: {
  //   '.+\\.(t|j)$': 'ts-jest',
  // },
  collectCoverageFrom: ['**/*.(t|j)s'],
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
  modulePathIgnorePatterns: ['containerOnly.*'],
};
