import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { environment } from './common/environment';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    transport: Transport.RMQ,
    options: {
      urls: [
        `amqp://${environment.MESSAGE_BROKER_HOST}:${environment.MESSAGE_BROKER_PORT}`,
      ],
      queue: 'user_creation_queue',
      queueOptions: {
        durable: false,
      },
    },
  });
  await app.startAllMicroservices();
  await app.listen(environment.SERVER_PORT);
  console.log(`Server started at PORT ${environment.SERVER_PORT}`)

}
bootstrap();
