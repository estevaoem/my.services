import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { environment } from './common/environment';
import { UserModule } from './user/user.module';

const mongoUri = `mongodb://${environment.DB_USER}:${environment.DB_PASSWORD}@${environment.DB_HOST}:${environment.DB_PORT}/${environment.DB_NAME}?authSource=admin`;

@Module({
  imports: [MongooseModule.forRoot(mongoUri), UserModule],
})
export class AppModule {}
