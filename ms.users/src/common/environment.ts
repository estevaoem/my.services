export const environment = {
  DB_USER: process.env.DB_USER ?? 'mongouser',
  DB_PASSWORD: process.env.DB_PASSWORD ?? 'secretpwd',
  DB_HOST: process.env.DB_HOST ?? 'users_ms_db',
  DB_PORT: process.env.DB_PORT ?? 27017,
  DB_NAME: process.env.DB_NAME ?? 'users',
  MESSAGE_BROKER_HOST: process.env.MESSAGE_BROKER_HOST ?? 'users_ms_messaging',
  MESSAGE_BROKER_PORT: process.env.MESSAGE_BROKER_PORT ?? 5672,
  MESSAGE_BROKER_USER: process.env.MESSAGE_BROKER_USER ?? 'guest',
  MESSAGE_BROKER_PASSWORD: process.env.MESSAGE_BROKER_PASSWORD ?? 'guest',
  SERVER_PORT: process.env.SERVER_PORT ? Number(process.env.SERVER_PORT) : 3000,
};
