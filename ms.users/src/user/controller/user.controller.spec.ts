/* eslint-disable @typescript-eslint/no-unused-vars */
import { UserController } from './user.controller';
import { IUserService } from '../service/iuser.service';
import { UserModel } from '../data/user.model';
import { Id, AvailablePlans } from '../../../../domain/types';
import { makeCreateUser } from '../../../../domain/dtos/create.user.factory';
import { Test, TestingModule } from '@nestjs/testing';
import { makeUserModel } from '../data/user.model.factory';
import { CreateUser } from '../../../../domain/dtos/create.user';

class UserServiceStub implements IUserService {
  async getUserByEmail(email: string): Promise<UserModel> {
    return makeUserModel();
  }

  async createUser(user: Omit<UserModel, 'id'>): Promise<void> {
    return;
  }

  async updateUserPlan(userId: Id, plan: AvailablePlans): Promise<void> {
    return;
  }
}

class ClientProxyStub {
  emit(pattern: string, data: any) {
    return;
  }
}

interface SutComponents {
  userService: IUserService;
  client: ClientProxyStub;
}

describe('UserController', () => {
  let sut: UserController;
  let components: SutComponents;
  let createdUser: CreateUser;
  let user: UserModel;

  beforeEach(async () => {
    user = makeUserModel();
    createdUser = makeCreateUser();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: 'UserService',
          useClass: UserServiceStub,
        },
        {
          provide: 'USER_SERVICE',
          useClass: ClientProxyStub,
        },
      ],
    }).compile();

    sut = module.get<UserController>(UserController);
    components = {
      userService: module.get<UserServiceStub>('UserService'),
      client: module.get<ClientProxyStub>('USER_SERVICE'),
    };
  });

  describe('Check user exists by provided email', () => {
    it('should call user service with correct value', async () => {
      const getUserSpy = jest.spyOn(components.userService, 'getUserByEmail');
      await sut.checkUserExistsByEmail('user_id');
      expect(getUserSpy).toHaveBeenCalledWith('user_id');
    });

    it('should return false if a user is not found', async () => {
      jest
        .spyOn(components.userService, 'getUserByEmail')
        .mockReturnValueOnce(null);
      const response = await sut.checkUserExistsByEmail('invalid_id');
      expect(response).toEqual(false);
    });

    it('should return true if a user is found', async () => {
      const response = await sut.checkUserExistsByEmail('existing_id');
      expect(response).toEqual(true);
    });
  });

  describe('Create user event', () => {
    it('should return failure if passwords unmatch', async () => {
      createdUser.passwordConfirmation = `unmatch${createdUser.password}`;
      const creationResult = await sut.createUser(createdUser);
      expect(creationResult).toEqual({
        status: 'failure',
        reason: 'password_unmatch',
      });
    });

    it('should call user service with correct value', async () => {
      const createUserSpy = jest.spyOn(components.userService, 'createUser');
      await sut.createUser(createdUser);
      const { email, password } = createdUser;
      expect(createUserSpy).toHaveBeenCalledWith({
        email,
        password,
        plan: 'free',
      });
    });

    it('should return failure on service failure', async () => {
      const error = new Error('error on UserService');
      jest
        .spyOn(components.userService, 'createUser')
        .mockImplementationOnce(
          async (user: Omit<UserModel, 'id'>): Promise<void> => {
            throw error;
          },
        );
      const creationResponse = await sut.createUser(createdUser);
      expect(creationResponse).toEqual({
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      });
    });

    it('should return ok on success', async () => {
      const response = await sut.createUser(createdUser);
      expect(response).toEqual({
        status: 'ok',
      });
    });
  });

  describe('Update user plan event', () => {
    it('should call user service with correct value', async () => {
      const updateUserPlanSpy = jest.spyOn(
        components.userService,
        'updateUserPlan',
      );
      await sut.updateUserPlan({ userId: user.id, plan: 'family' });
      expect(updateUserPlanSpy).toHaveBeenCalledWith(user.id, 'family');
    });

    it('should return failure if service throws', async () => {
      const error = new Error('user service error');
      jest
        .spyOn(components.userService, 'updateUserPlan')
        .mockImplementationOnce(async (): Promise<void> => {
          throw error;
        });
      const updateResult = await sut.updateUserPlan({
        userId: user.id,
        plan: 'family',
      });
      expect(updateResult).toEqual({
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      });
    });

    it('should return ok on success', async () => {
      const response = await sut.updateUserPlan({
        userId: user.id,
        plan: 'family',
      });
      expect(response).toEqual({ status: 'ok' });
    });
  });
});
