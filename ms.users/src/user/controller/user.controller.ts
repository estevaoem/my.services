import { Controller, Inject } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { IUserService } from '../service/iuser.service';
import { CreateUser } from '../../../../domain/dtos/create.user';
import { OperationStatus } from '../../../../domain/dtos/operation';
import { AvailablePlans, Id } from '../../../../domain/types';

@Controller()
export class UserController {
  constructor(
    @Inject('UserService') private readonly userService: IUserService,
  ) {}

  @MessagePattern({ cmd: 'check_user_exists' })
  async checkUserExistsByEmail(email: string): Promise<boolean> {
    let userExists = false;
    const user = await this.userService.getUserByEmail(email);

    if (user) {
      userExists = true;
    }

    return userExists;
  }

  @MessagePattern({ cmd: 'create_user' })
  async createUser(createUser: CreateUser): Promise<OperationStatus> {
    try {
      const { password, passwordConfirmation } = createUser;
      if (!(password === passwordConfirmation)) {
        return {
          status: 'failure',
          reason: 'password_unmatch',
        };
      }

      await this.userService.createUser({
        email: createUser.email,
        password: createUser.password,
        plan: 'free',
      });
      return {
        status: 'ok',
      };
    } catch (error) {
      return {
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      };
    }
  }

  @MessagePattern({ cmd: 'payment_confirmation' })
  async updateUserPlan(data: {
    userId: Id;
    plan: AvailablePlans;
  }): Promise<OperationStatus> {
    try {
      await this.userService.updateUserPlan(data.userId, data.plan);
      return {
        status: 'ok',
      };
    } catch (error) {
      return {
        status: 'failure',
        reason: 'component_failure',
        stack: error,
      };
    }
  }
}
