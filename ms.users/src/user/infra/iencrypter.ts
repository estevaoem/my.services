export interface IEncrypter {
  encrypt(data: string): Promise<string>;
}
