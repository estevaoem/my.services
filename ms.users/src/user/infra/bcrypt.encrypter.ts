import { IEncrypter } from './iencrypter';
import bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BcryptEncrypter implements IEncrypter {
  async encrypt(data: string): Promise<string> {
    const salt = await bcrypt.genSalt(12);
    return await bcrypt.hash(data, salt);
  }
}
