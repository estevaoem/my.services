/* eslint-disable @typescript-eslint/no-unused-vars */
import { BcryptEncrypter } from './bcrypt.encrypter';
import bcrypt from 'bcrypt';

describe('BcryptEncrypter', () => {
  let sut: BcryptEncrypter;
  let generatedSalt: string;

  beforeAll(async () => {
    generatedSalt = await bcrypt.genSalt(12);
  });

  beforeEach(async () => {
    sut = new BcryptEncrypter();
    jest
      .spyOn(bcrypt, 'genSalt')
      .mockImplementationOnce(async (): Promise<string> => {
        return generatedSalt;
      });
  });

  it('should call hash with correct values', async () => {
    const hashSpy = jest.spyOn(bcrypt, 'hash');
    const genSaltSpy = jest.spyOn(bcrypt, 'genSalt');
    await sut.encrypt('plain_password');
    expect(genSaltSpy).toHaveBeenCalledWith(12);
    expect(hashSpy).toHaveBeenCalledWith('plain_password', generatedSalt);
  });
  it('should throw if bcrypt throws', async () => {
    jest
      .spyOn(bcrypt, 'genSalt')
      .mockImplementationOnce(async (): Promise<string> => {
        throw new Error('any error');
      });
    const e1 = sut.encrypt('plain_password');
    expect(e1).rejects.toThrow();
    jest
      .spyOn(bcrypt, 'hash')
      .mockImplementationOnce(async (): Promise<string> => {
        throw new Error('any error');
      });
    const e2 = sut.encrypt('plain_password');
    expect(e2).rejects.toThrow();
  });
  it('should return an encrypted string on success', async () => {
    const response = await sut.encrypt('plain_password');
    expect(await bcrypt.compare('other_password', response)).toEqual(false);
    expect(await bcrypt.compare('plain_password', response)).toEqual(true);
  });
});
