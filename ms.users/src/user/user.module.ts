import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './controller/user.controller';
import { UserSchema } from './data/mongo/user.model.mongodb';
import { MongoUserRepository } from './data/mongo/mongodb.user.repository';
import { UserService } from './service/user.service';
import { BcryptEncrypter } from './infra/bcrypt.encrypter';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Users',
        schema: UserSchema,
        collection: 'users',
      },
    ]),
  ],
  controllers: [UserController],
  providers: [
    {
      provide: 'UserService',
      useClass: UserService,
    },
    {
      provide: 'UserRepository',
      useClass: MongoUserRepository,
    },
    {
      provide: 'Encrypter',
      useClass: BcryptEncrypter,
    },
  ],
})
export class UserModule {}
