import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Id } from '../../../../../domain/types';
import { IUserRepository } from '../iuser.repository';
import { UserModel } from '../user.model';
import { UserDocument } from './user.model.mongodb';

@Injectable()
export class MongoUserRepository implements IUserRepository {
  constructor(
    @InjectModel('Users') private readonly usersModel: Model<UserDocument>,
  ) {}
  async getByEmail(email: string): Promise<UserModel | null> {
    const user = await this.usersModel.findOne({
      email,
    });

    if (user) {
      user.id = user._id.toString();
    }

    return user;
  }
  async create(user: Omit<UserModel, 'id'>): Promise<void> {
    await this.usersModel.insertMany([user]);
  }
  async update(userId: Id, update: Partial<Omit<UserModel, 'id'>>) {
    await this.usersModel.updateOne({ id: userId, $set: update });
  }
}
