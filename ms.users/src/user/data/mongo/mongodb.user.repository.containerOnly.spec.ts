import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { connect, Connection, Model } from 'mongoose';
import { MongoUserRepository } from './mongodb.user.repository';
import { MongoDbUserModel, UserSchema } from './user.model.mongodb';
import { environment } from '../../../common/environment';
import { UserModel } from '../user.model';
import { faker } from '@faker-js/faker';

const mongoUri = `mongodb://${environment.DB_USER}:${environment.DB_PASSWORD}@${environment.DB_HOST}:${environment.DB_PORT}/${environment.DB_NAME}?authSource=admin`;

class MongoHelper {
  private mongoConnection: Connection;
  usersModel: Model<MongoDbUserModel>;

  async setup() {
    this.mongoConnection = (await connect(mongoUri)).connection;
    this.usersModel = this.mongoConnection.model('Users', UserSchema);
  }

  async clearDb(): Promise<void> {
    await this.usersModel.deleteMany({});
  }

  async getUsers(): Promise<UserModel[]> {
    return await this.usersModel.find();
  }

  async insertUser(user: Omit<UserModel, 'id'>): Promise<void> {
    await this.usersModel.insertMany([user]);
  }
}

const makeUser = (): Omit<UserModel, 'id'> => ({
  email: faker.internet.email(),
  password: faker.internet.password(),
  plan: 'free',
});

describe('MongoUserRepository', () => {
  let sut: MongoUserRepository;
  const mongoHelper = new MongoHelper();

  beforeAll(async () => {
    await mongoHelper.setup();
  });

  afterAll(async () => {
    await mongoHelper.clearDb();
    await mongoHelper.insertUser({
      email: 'api_user@mail.com',
      password: 'user_pwd',
      plan: 'individual',
    });
  });

  beforeEach(async () => {
    await mongoHelper.clearDb();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MongoUserRepository,
        {
          provide: getModelToken('Users'),
          useValue: mongoHelper.usersModel,
        },
      ],
    }).compile();

    sut = module.get<MongoUserRepository>(MongoUserRepository);
  });

  describe('Get by email', () => {
    it('should call findOne with correct value', async () => {
      const email = faker.internet.email();
      const findOneSpy = jest.spyOn(mongoHelper.usersModel, 'findOne');
      await sut.getByEmail(email);
      expect(findOneSpy).toHaveBeenCalledWith({ email });
    });

    it('should add the id field for the found user', async () => {
      const madeUser = makeUser();
      await mongoHelper.insertUser(madeUser);
      const user = await sut.getByEmail(madeUser.email);
      expect(user.id).toEqual((user as any)._id);
    });

    it('should return null if no user is found', async () => {
      const email = faker.internet.email();
      const user = await sut.getByEmail(email);
      expect(user).toBe(null);
    });

    it('should throw if findOne throws', async () => {
      jest
        .spyOn(mongoHelper.usersModel, 'findOne')
        .mockImplementationOnce(() => {
          throw new Error('Error on findOne');
        });
      const email = faker.internet.email();
      const user = sut.getByEmail(email);
      expect(user).rejects.toThrow();
    });

    it('should return stored user on success', async () => {
      const user = makeUser();
      await mongoHelper.insertUser(user);
      const storedUser = await sut.getByEmail(user.email);
      for (const key in user) {
        expect(storedUser[key]).toEqual(user[key]);
      }
    });
  });

  describe('Create', () => {
    let user: Omit<UserModel, 'id'>;

    beforeEach(async () => {
      user = makeUser();
    });

    it('should call insertMany with correct value', async () => {
      const insertManySpy = jest.spyOn(mongoHelper.usersModel, 'insertMany');
      await sut.create(user);
      expect(insertManySpy).toHaveBeenCalledWith([user]);
    });

    it('should throw if insertMany throws', async () => {
      jest
        .spyOn(mongoHelper.usersModel, 'insertMany')
        .mockImplementationOnce(() => {
          throw new Error('Error on insertMany');
        });
      const userCreation = sut.create(user);
      expect(userCreation).rejects.toThrow();
    });

    it('should create a user on success', async () => {
      await sut.create(user);
      expect((await mongoHelper.getUsers()).length).toEqual(1);
    });
  });

  describe('Update', () => {
    it('should call updateOne with correct value', async () => {
      const updateOneSpy = jest.spyOn(mongoHelper.usersModel, 'updateOne');
      const id = faker.database.mongodbObjectId();
      await sut.update(id, {
        plan: 'enterprise',
      });
      expect(updateOneSpy).toHaveBeenCalledWith({
        id,
        $set: { plan: 'enterprise' },
      });
    });

    it('should throw if updateOne throws', async () => {
      jest
        .spyOn(mongoHelper.usersModel, 'updateOne')
        .mockImplementationOnce(() => {
          throw new Error('Error on updateOne');
        });
      const updateResult = sut.update(faker.database.mongodbObjectId(), {
        plan: 'enterprise',
      });
      expect(updateResult).rejects.toThrow();
    });

    it('should update an user on success', async () => {
      await mongoHelper.insertUser({
        email: 'api_user@mail.com',
        password: 'user_pwd',
        plan: 'individual',
      });
      const storedUser = await mongoHelper.getUsers();
      await sut.update((storedUser as any)._id, {
        email: 'new_email@mail.com',
      });
      const updatedstoredUser = await mongoHelper.getUsers();
      expect((updatedstoredUser as UserModel[])[0].email).toEqual(
        'new_email@mail.com',
      );
    });
  });
});
