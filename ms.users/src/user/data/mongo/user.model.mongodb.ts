import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { AvailablePlans } from '../../../../../domain/types';
import { UserModel } from '../user.model';

@Schema()
export class MongoDbUserModel implements UserModel {
  @Prop({ type: mongoose.Schema.Types.ObjectId })
  id: string;

  @Prop({ type: String, required: true })
  email: string;

  @Prop({ type: String, required: true })
  password: string;

  @Prop({ type: String, required: true })
  plan: AvailablePlans;
}

export type UserDocument = HydratedDocument<MongoDbUserModel>;
export const UserSchema = SchemaFactory.createForClass(MongoDbUserModel);
