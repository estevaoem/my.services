import { faker } from "@faker-js/faker";
import { UserModel } from "./user.model";

export const makeUserModel = (): UserModel => ({
  id: faker.database.mongodbObjectId(),
  email: faker.internet.email(),
  password: faker.internet.password(),
  plan: 'free'
})