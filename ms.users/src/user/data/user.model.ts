import { Id, AvailablePlans } from "../../../../domain/types";

export interface UserModel {
  id: Id;
  email: string;
  password: string;
  plan: AvailablePlans;
}
