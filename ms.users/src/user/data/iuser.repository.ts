import { Id } from '../../../../domain/types';
import { UserModel } from './user.model';

export interface IUserRepository {
  getByEmail(email: string): Promise<UserModel | null>;
  create(user: Omit<UserModel, 'id'>): Promise<void>;
  update(userId: Id, update: Partial<Omit<UserModel, 'id'>>): Promise<void>;
}
