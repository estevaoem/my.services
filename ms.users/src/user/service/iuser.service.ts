import { AvailablePlans, Id } from '../../../../domain/types';
import { UserModel } from '../data/user.model';

export interface IUserService {
  getUserByEmail(email: string): Promise<UserModel | null>;
  createUser(user: Omit<UserModel, 'id'>): Promise<void>;
  updateUserPlan(userId: Id, plan: AvailablePlans): Promise<void>;
}
