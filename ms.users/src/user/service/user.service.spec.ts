/* eslint-disable @typescript-eslint/no-unused-vars */
import { faker } from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { Id } from '../../../../domain/types';
import { IUserRepository } from '../data/iuser.repository';
import { UserModel } from '../data/user.model';
import { IEncrypter } from '../infra/iencrypter';
import { UserService } from './user.service';

const makeUser = (): UserModel => ({
  id: faker.datatype.uuid(),
  email: faker.internet.email(),
  password: faker.internet.password(),
  plan: 'free',
});

class UserRepositoryStub implements IUserRepository {
  public static user = makeUser();

  async getByEmail(email: string): Promise<UserModel | null> {
    return UserRepositoryStub.user;
  }

  async create(user: Omit<UserModel, 'id'>): Promise<void> {
    return;
  }
  async update(
    userId: Id,
    update: Partial<Omit<UserModel, 'id'>>,
  ): Promise<void> {
    return;
  }
}

class EncrypterStub implements IEncrypter {
  async encrypt(data: string): Promise<string> {
    return 'hashed_string';
  }
}

interface SutComponents {
  userRepository: IUserRepository;
}

describe('UserService', () => {
  let sut: UserService;
  let components: SutComponents;
  let user: UserModel;

  beforeEach(async () => {
    user = makeUser();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: 'UserRepository',
          useClass: UserRepositoryStub,
        },
        {
          provide: 'Encrypter',
          useClass: EncrypterStub,
        },
      ],
    }).compile();

    sut = module.get<UserService>(UserService);
    components = {
      userRepository: module.get<UserRepositoryStub>('UserRepository'),
    };
  });

  describe('Get user by email', () => {
    it('should call user respository with correct value', async () => {
      const getByEmailSpy = jest.spyOn(components.userRepository, 'getByEmail');
      await sut.getUserByEmail('any_email@mail.com');
      expect(getByEmailSpy).toHaveBeenCalledWith('any_email@mail.com');
    });

    it('should return null if a user is not found', async () => {
      jest
        .spyOn(components.userRepository, 'getByEmail')
        .mockReturnValueOnce(null);
      const response = await sut.getUserByEmail('invalid_email@mail.com');
      expect(response).toEqual(null);
    });

    it('should throw if userRepository throws', async () => {
      jest
        .spyOn(components.userRepository, 'getByEmail')
        .mockImplementationOnce(
          async (email: string): Promise<UserModel | null> => {
            throw new Error('any error');
          },
        );
      const response = sut.getUserByEmail('any_email@mail.com');
      expect(response).rejects.toThrow();
    });

    it('should return found user on success', async () => {
      const response = await sut.getUserByEmail(UserRepositoryStub.user.email);
      expect(response).toEqual(UserRepositoryStub.user);
    });
  });

  describe('Create user', () => {
    it('should call user respository with correct value', async () => {
      const createSpy = jest.spyOn(components.userRepository, 'create');
      const { id, ...rest } = user;
      await sut.createUser(rest);
      expect(createSpy).toHaveBeenCalledWith(rest);
    });

    it('should enter a user with free plan', async () => {
      const createSpy = jest.spyOn(components.userRepository, 'create');
      const { id, ...rest } = user;
      rest.plan = 'individual';
      await sut.createUser(rest);
      const { plan, ...userAttr } = rest;
      expect(createSpy).toHaveBeenCalledWith({ ...userAttr, plan: 'free' });
    });

    it('should encrypt the user password', async () => {
      const createSpy = jest.spyOn(components.userRepository, 'create');
      const { id, ...rest } = user;
      await sut.createUser(rest);
      const callArgs = createSpy.mock.calls[0][0];
      expect(callArgs.password).toEqual('hashed_string');
    });

    it('should throw if userRepository throws', async () => {
      jest
        .spyOn(components.userRepository, 'create')
        .mockImplementationOnce(async (user: Omit<UserModel, 'id'>) => {
          throw new Error('any error');
        });
      const response = sut.createUser(user);
      expect(response).rejects.toThrow();
    });

    it('should return nothing on success', async () => {
      const response = await sut.createUser(user);
      expect(response).toEqual(undefined);
    });
  });

  describe('Update user plan', () => {
    it('should call user respository with correct value', async () => {
      const updateSpy = jest.spyOn(components.userRepository, 'update');
      await sut.updateUserPlan(user.id, 'individual');
      expect(updateSpy).toHaveBeenCalledWith(user.id, { plan: 'individual' });
    });

    it('should throw if userRepository throws', async () => {
      jest
        .spyOn(components.userRepository, 'update')
        .mockImplementationOnce(
          async (
            userId: Id,
            update: Partial<Omit<UserModel, 'id'>>,
          ): Promise<void> => {
            throw new Error('any error');
          },
        );
      const response = sut.updateUserPlan(user.id, 'individual');
      expect(response).rejects.toThrow();
    });

    it('should return nothing on success', async () => {
      const response = await sut.updateUserPlan(user.id, 'family');
      expect(response).toEqual(undefined);
    });
  });
});
