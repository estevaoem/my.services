import { Inject, Injectable } from '@nestjs/common';
import { IUserService } from './iuser.service';
import { UserModel } from '../data/user.model';
import { IUserRepository } from '../data/iuser.repository';
import { IEncrypter } from '../infra/iencrypter';
import { AvailablePlans, Id } from '../../../../domain/types';

@Injectable()
export class UserService implements IUserService {
  constructor(
    @Inject('UserRepository') private readonly userRepository: IUserRepository,
    @Inject('Encrypter') private readonly encrypter: IEncrypter,
  ) {}
  async getUserByEmail(email: string): Promise<UserModel | null> {
    return await this.userRepository.getByEmail(email);
  }

  async createUser(user: Omit<UserModel, 'id'>): Promise<void> {
    user.plan = 'free';
    user.password = await this.encrypter.encrypt(user.password);
    await this.userRepository.create(user);
  }

  async updateUserPlan(userId: Id, plan: AvailablePlans): Promise<void> {
    await this.userRepository.update(userId, { plan });
  }
}
