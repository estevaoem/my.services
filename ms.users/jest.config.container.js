/* eslint-disable @typescript-eslint/no-var-requires */
const config = require('./jest.config');
config.modulePathIgnorePatterns = [];
config.testRegex = '.*\\.containerOnly\\.spec|test\\.ts$';
module.exports = config;
