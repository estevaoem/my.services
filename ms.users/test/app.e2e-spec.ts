import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { MessagingModule } from './messaging/messaging.module';
import { TestMessagingService } from './messaging/messaging.service';
import { environment } from '../src/common/environment';
import { connect, Connection, Model } from 'mongoose';
import {
  MongoDbUserModel,
  UserSchema,
} from '../src/user/data/mongo/user.model.mongodb';
import { UserModel } from '../src/user/data/user.model';
import { makeUserModel } from '../src/user/data/user.model.factory';
import { makeCreateUser } from '../../domain/dtos/create.user.factory';
import { AvailablePlans } from '../../domain/types';

const mongoUri = `mongodb://${environment.DB_USER}:${environment.DB_PASSWORD}@${environment.DB_HOST}:${environment.DB_PORT}/${environment.DB_NAME}?authSource=admin`;

class MongoHelper {
  mongoConnection: Connection;
  usersModel: Model<MongoDbUserModel>;

  async setup() {
    this.mongoConnection = (await connect(mongoUri)).connection;
    this.usersModel = this.mongoConnection.model(
      'MongoDbUserModel',
      UserSchema,
      'users',
    );
  }

  async clearDb(): Promise<void> {
    await this.usersModel.deleteMany({});
  }

  async getUsers(): Promise<UserModel[]> {
    return await this.usersModel.find();
  }

  async insertUser(user: UserModel): Promise<void> {
    await this.usersModel.insertMany([user]);
  }
}

const mongoHelper = new MongoHelper();

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let messaging: INestApplication;
  let messagingService: TestMessagingService;
  let user: UserModel;

  beforeAll(async (): Promise<void> => {
    await mongoHelper.setup();
    await mongoHelper.clearDb();
  });

  beforeEach(async () => {
    user = makeUserModel();

    await mongoHelper.clearDb();
    await mongoHelper.insertUser(user);

    const messagingModuleFixture: TestingModule =
      await Test.createTestingModule({
        imports: [MessagingModule],
      }).compile();

    messaging = messagingModuleFixture.createNestApplication();
    await messaging.startAllMicroservices();
    await messaging.init();

    messagingService =
      messaging.get<TestMessagingService>(TestMessagingService);

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('User exists by email', () => {
    it('should return true if a user exists', async () => {
      const userExists = await messagingService.sendMessage(
        { cmd: 'check_user_exists' },
        user.email,
      );
      expect(userExists).toEqual(true);
    });

    it("should return false if a user doesn't exists", async () => {
      const userExists = await messagingService.sendMessage(
        { cmd: 'check_user_exists' },
        'mail@mail.com',
      );
      expect(userExists).toEqual(false);
    });
  });

  describe('Create user', () => {
    it('should create a user on success', async () => {
      await mongoHelper.clearDb();

      const createUser = makeCreateUser();
      const userCreationResult = await messagingService.sendMessage(
        { cmd: 'create_user' },
        createUser,
      );
      const users = await mongoHelper.getUsers();
      expect(users[0].email).toEqual(createUser.email);
      expect(userCreationResult).toEqual({ status: 'ok' });
    });
  });

  describe('Update user plan', () => {
    it('should update user plan on success', async () => {
      const users = await mongoHelper.getUsers();
      expect(users[0].plan).toEqual('free');
      const userId: string = (users[0] as any)._id;
      const plan: AvailablePlans = 'family';

      const updateUserPlanResult = await messagingService.sendMessage(
        { cmd: 'payment_confirmation' },
        {
          userId,
          plan,
        },
      );
      const updatedusers = await mongoHelper.getUsers();
      expect(updatedusers.length).toEqual(1);
      expect(updatedusers[0].plan).toEqual('family');
      expect(updateUserPlanResult).toEqual({ status: 'ok' });
    });
  });
});
