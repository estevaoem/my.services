import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class TestMessagingService {
  constructor(@Inject('TEST_SERVICE') private client: ClientProxy) {}

  async sendMessage<ResponseType>(
    pattern: any,
    data: any,
  ): Promise<ResponseType> {
    return await this.client.send<ResponseType>(pattern, data).toPromise();
  }
}
