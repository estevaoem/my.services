import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { environment } from '../../src/common/environment';
import { TestMessagingService } from './messaging.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'TEST_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [
            `amqp://${environment.MESSAGE_BROKER_USER}:${environment.MESSAGE_BROKER_PASSWORD}@${environment.MESSAGE_BROKER_HOST}:${environment.MESSAGE_BROKER_PORT}`,
          ],
          queue: 'user_creation_queue',
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  providers: [TestMessagingService],
})
export class MessagingModule {}
