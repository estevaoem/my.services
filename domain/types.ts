export type Id = string | number;
export type AvailablePlans = 'free' | 'individual' | 'family' | 'enterprise';

export interface PayloadWithId {
  id: Id
  [property: string]: any
}
