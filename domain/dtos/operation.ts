type Results = 'ok' | 'failure';
type FailureReasons =
  | 'user_exists'
  | 'password_unmatch'
  | 'component_failure'
  | 'invalid_token'
  | 'invalid_user';

export interface OperationStatus {
  status: Results;
  payload?: { [parameter: string]: any };
  reason?: FailureReasons;
  stack?: any;
}
