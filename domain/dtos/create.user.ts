export interface CreateUserDto {
  password: string;
  passwordConfirmation: string;
}

export interface CreateUser extends CreateUserDto {
  email: string;
}
