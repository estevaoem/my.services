import { faker } from '@faker-js/faker';
import { CreateUser, CreateUserDto } from './create.user';

export const makeCreateUserDto = (): CreateUserDto => {
  const password = faker.internet.password();
  return {
    password,
    passwordConfirmation: password,
  };
};

export const makeCreateUser = (): CreateUser => {
  const password = faker.internet.password();
  return {
    email: faker.internet.email(),
    password,
    passwordConfirmation: password,
  };
};
