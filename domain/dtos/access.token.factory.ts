import { AccessTokenDto } from './access.token';

export const makeAccessTokenDto = (): AccessTokenDto => ({
  accessToken: 'valid_access_token',
});
