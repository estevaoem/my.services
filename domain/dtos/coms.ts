type AvailableComTypes = 'token';
type AvailableComVias = 'email';

export interface SendComunication {
  type: AvailableComTypes;
  via: AvailableComVias;
  payload: any;
}
